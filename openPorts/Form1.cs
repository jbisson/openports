﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TcpLib;

namespace openPorts
{
    public partial class Form1 : Form
    {
        delegate void LogCallback(String text);

        public Form1()
        {
            InitializeComponent();

            Log.LogEvent += logListener;

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    Log.debug("Found ip: " + ip.ToString());
                    
                    if (tbIpBinding.Text == "")
                        tbIpBinding.Text = ip.ToString();                    
                }
            }
        }

        public void logListener(String strLog)
        {
            if (tbLogs.InvokeRequired)
            {
                LogCallback d = new LogCallback(logListener);
                this.Invoke(d, new object[] { strLog });
            }
            else
            {
                tbLogs.AppendText(strLog + "\r\n");
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            int from, to = 0;

            try
            {
                from = Convert.ToInt32(tbFrom.Text);
                to = Convert.ToInt32(tbTo.Text);
                
            } catch (FormatException ex)
            {
                MessageBox.Show("Enter decimal values in from/to port boxes.");
                Log.error(ex.ToString());
                return;
            }

            if (from > to)
            {
                MessageBox.Show("From port need to be less than to port.");
                return;
            }

            int error = 0;
            StringBuilder strPortError = new StringBuilder();
            for (int i = from; i <= to; i++)
            {
                EchoServiceProvider myEchoServiceProvider = new EchoServiceProvider();
                TcpServer myServer = new TcpServer(myEchoServiceProvider, tbIpBinding.Text, i);
                if (!myServer.Start())
                {
                    error += 1;
                    strPortError.Append(i + " ");
                    
                }
            }

            Log.info("Added: " + (to-from-error) + " ports");
            if (error >= 0) {
                Log.error("Couln't bind to these port(s) - port in use?: " + strPortError.ToString());
            }


        }

        private void tbLogs_TextChanged(object sender, EventArgs e)
        {

        }




        

    }
}
