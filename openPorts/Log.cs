﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace openPorts
{
    public static class Log
    {
        public delegate void LogDelegate(String log);
        public static event LogDelegate LogEvent;

        public static void error(String strError)
        {
            log("ERROR", strError);
        }

        public static void info(String strError)
        {
            log("INFO", strError);
        }

        public static void debug(String strError)
        {
            log("DEBUG", strError);
        }


        private static void log(String logLevel, String log)
        {
            DateTime date = DateTime.Now;
            String strLog = "[" + DateTime.Now.ToString() + "]  " + logLevel + ": " + log + "\n";

            try
            {
                StreamWriter SWrite = new StreamWriter("log.txt", true, System.Text.Encoding.UTF8);
                SWrite.WriteLine(strLog);
                SWrite.Close();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("ERROR: Cannot save log: " + ex);
            }

            Console.WriteLine(strLog);

            if (LogEvent.GetInvocationList().Length > 0)
            {
                LogEvent(strLog);
            }
        }
    }
}
